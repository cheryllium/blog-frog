{ pkgs ? import <nixpkgs> {} }:
pkgs.mkShell {
    # nativeBuildInputs is usually what you want -- tools you need to run
    nativeBuildInputs = [ pkgs.php81Packages.composer pkgs.sqlite pkgs.nodejs
      (pkgs.php81.buildEnv {
          extraConfig = ''
             upload_max_filesize = 10000M
             post_max_size = 10000M
          '';
       })];
}