<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\BlogFrogController; 
use App\Models\FrogPost;
use App\Models\Tag;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Fix base URLs for routes behind reverse proxy
$proxy_url    = env('PROXY_URL');
$proxy_scheme = env('PROXY_SCHEME');
if (!empty($proxy_url)) {
   URL::forceRootUrl($proxy_url);
}
if (!empty($proxy_scheme)) {
   URL::forceScheme($proxy_scheme);
}

// Application routes
Route::get('/', [BlogFrogController::class, 'index'])->name('blogfrog.index');
Route::get('/rss', [BlogFrogController::class, 'rss'])->name('blogfrog.rss');
Route::get('/tags', [BlogFrogController::class, 'tags'])->name('blogfrog.tags');
Route::get('/tag/{tag}', [BlogFrogController::class, 'tagged'])->name('blogfrog.tagged');
Route::get('/page/{slug}', [BlogFrogController::class, 'post'])->name('blogfrog.page');
Route::get('/{slug}', [BlogFrogController::class, 'post'])->name('blogfrog.post'); 

