<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Models\FrogPost;
use Illuminate\Support\Facades\Blade;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('markdown', function($expression) {
            return "<?php echo (new \App\ExtendedParsedown)->text($expression); ?>";
        }); 
    }
}
