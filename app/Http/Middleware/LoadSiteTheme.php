<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

use View;

class LoadSiteTheme
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $theme = config('blogfrog.site_theme');
        
        View::share('layoutComponent', "themes.{$theme}.layout");
        View::share('postComponent', "themes.{$theme}.post");
        View::share('postPreviewComponent', "themes.{$theme}.post_preview");
        View::share('paginationComponent', "themes.{$theme}.pagination");
        
        return $next($request);
    }
}
