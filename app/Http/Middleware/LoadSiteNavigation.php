<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

use App\Models\FrogPost;
use View;

class LoadSiteNavigation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        // Send pages to all views for site navigation
        $results = FrogPost::where('type', 'page')->get(); 
        $pages = [];
        foreach($results as $page) {
            $pages[] = [
                'title' => $page->getFileInfo()['title'],
                'slug' => $page->slug,
            ];
        }
        View::share('pages', $pages);
        
        return $next($request);
    }
}
