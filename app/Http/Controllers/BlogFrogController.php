<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\FrogPost;
use App\Models\Tag;

use View;

class BlogFrogController extends Controller {

    private function listPosts($paginator) {
        $posts = [];
        foreach($paginator as $post) {
            $posts[] = $post->getPostObject();
        }

        return View::make('index', [
            'posts' => $posts,
            'paginator' => $paginator, 
        ]);        
    }

    private function displayPost($post) {
        return View::make('post', [
            'post' => $post->getPostObject(),
        ]);
    }
    
    /**
     * Displays the home page with a listing of blog posts
     */
    public function index(Request $request) {
        $posts_paginator = FrogPost::where('type', 'post')
                                   ->orderBy('created_at', 'desc')
                                   ->simplePaginate(3)
                                   ->setPath('');
        return $this->listPosts($posts_paginator);
    }

    /**
     * Displays all posts tagged with the given tag.
     */
    public function tagged(string $tag) {
        $tag = Tag::where('tag', $tag)->first();
        $posts_paginator = $tag->posts()->orderBy('created_at', 'desc')
                               ->simplePaginate(3)
                               ->setPath('');
        return $this->listPosts($posts_paginator);
    }

    /** 
     * Displays a single post
     */
    public function post(string $slug) {
        $post = FrogPost::where('slug', $slug)->firstOrFail();
        return $this->displayPost($post);
    }

    /**
     * Displays all tags with at least one post attached
     */
    public function tags() {
        $tags = Tag::has('posts')->orderBy('tag')->get()->pluck('tag')->toArray();
        return response()->view('tags', [
            'tags' => $tags,
        ]); 
    }
    
    /**
     * Displays the RSS feed.
     */
    public function rss() {
        $posts = FrogPost::where('type', 'post')
                         ->orderBy('created_at', 'desc')
                         ->limit(25)
                         ->get();
        
        return response()->view('feed', [
            'posts' => $posts,
        ])->header('Content-Type', 'application/xml');
    }
}
