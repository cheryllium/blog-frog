<?php

namespace App;

class ExtendedParsedown extends \Parsedown {

    protected function blockFencedCode($Line) {
        $block = parent::blockFencedCode($Line);

        if(
            $block &&
            array_key_exists('element', $block)
            && array_key_exists('text', $block['element'])
            && array_key_exists('attributes', $block['element']['text'])
            && array_key_exists('class', $block['element']['text']['attributes'])
        ) {
            $block['element']['text']['attributes'] = [
                'data-language' => explode(
                    '-', $block['element']['text']['attributes']['class']
                )[1]
            ];
        }

        return $block;
    }
}
