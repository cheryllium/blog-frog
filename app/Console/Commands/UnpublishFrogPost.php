<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\FrogPost;
use Storage;

class UnpublishFrogPost extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'frog:unpublish {slug}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Unpublishes a blog post (or page)';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $slug = trim($this->argument('slug'));

        // Check that there is a post with this slug
        if(!FrogPost::where('slug', $slug)->exists()) {
            $this->error("No post found with the slug: {$slug}");
            return 1; 
        }

        // Confirm that they really want to unpublish
        if($this->confirm("Unpublishing will move the post back to your drafts folder. Really unpublish the post?")) {
            // Move post back to drafts folder
            Storage::move("/published/{$slug}.md", "/drafts/{$slug}.md");

            // Delete DB row
            $post = FrogPost::where('slug', $slug)->first();
            $post->delete();

            $this->line("Post unpublished.");
        } 
        
        return Command::SUCCESS;
    }
}
