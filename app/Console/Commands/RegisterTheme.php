<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;

class RegisterTheme extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'frog:register-theme {theme}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Register a site theme';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $theme = trim($this->argument('theme'));

        // Symlink Blade template files
        $this->line("Symlinking theme templates...");
        $process = new Process([
            'ln', '-s',
            "../../../../themes/{$theme}/blade",
            "resources/views/components/themes/{$theme}"
        ]);
        $result = $process->run();
        if($result === 0) {
            $this->line("Symlink created.");
        } else {
            $this->error("An error occurred creating the symlink: " . $process->getOutput());
            return 1; 
        }

        // Symlink static assets (CSS, images, etc.)
        $this->line("Symlinking theme assets...");
        $process = new Process([
            'ln', '-s',
            "../../themes/{$theme}/assets",
            "public/themes/{$theme}"
        ]);
        $result = $process->run(); 
        if($result === 0) {
            $this->line("Symlink created.");
        } else {
            $this->error("An error occurred creating the symlink: " . $process->getOutput());
            // @todo - undo the previous symlink so this doesn't exit in partially completed state.
            return 1; 
        }

        // We did it!
        $this->info("Success!! Theme '{$theme}' was registered. You can now use this theme by setting SITE_THEME={$theme} in the project's .env file.");
        
        return Command::SUCCESS;
    }
}
