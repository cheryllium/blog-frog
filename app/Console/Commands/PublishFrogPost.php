<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Storage;

use App\Models\FrogPost;
use App\Models\Tag;

class PublishFrogPost extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'frog:publish {slug} {--page : Add to publish as a page instead of a blog post}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Publishes a blog post (or page)';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $slug = trim($this->argument('slug'));
        $storage_path = "/drafts/{$slug}.md";

        // Check that there is not already a post with this slug
        if(FrogPost::where('slug', $slug)->exists()) {
            $this->error("There is already a post with that slug. Post slugs must be unique.");
            return 1; 
        }
        
        // Check that the draft file exists
        if(Storage::missing($storage_path)) {
            $this->error("The draft file (/storage/app/drafts/{$slug}.md) cannot be found.");
            return 1; 
        }

        // Check that the draft file is in valid format
        $file_contents = Storage::get($storage_path);
        $firstline = trim(explode("\n", $file_contents, 2)[0]);
        if(!str_starts_with($firstline, "#")) {
            $this->error("Error: Invalid post format - The first line of your file must begin with # (what follows it will be used as the post title.)");
            return 1; 
        }

        // Check that the blog post has a valid title
        $title = trim(explode("#", $firstline)[1]);
        if($title == "") {
            $this->error("Error: Your blog post title cannot be blank!");
            return 1; 
        }

        // Move the file
        Storage::move($storage_path, "/published/{$slug}.md");

        // It is valid. Create the post
        $type = 'post';
        if($this->option('page')) {
            $type = 'page'; 
        }
        $post = new FrogPost([
            'slug' => $slug,
            'type' => $type,
        ]);
        $post->save(); 

        // Print some information
        $this->line("Publishing {$post->type} with the following attributes...");
        $this->line("Title: {$title}");
        $this->line("Slug: {$post->slug}");

        // Add tags
        if($type == 'post' && $this->confirm("Would you like to add tags to the post?")) {
            $tags = $this->ask("Enter tags separated by commas");
            $tags = explode(',', $tags); 
            foreach($tags as $tag) {
                $tag = strtolower(trim($tag));

                if(Tag::where('tag', $tag)->exists()) {
                    $tag = Tag::where('tag', $tag)->first(); 
                } else {
                    $tag = new Tag(['tag' => $tag]);
                    $tag->save(); 
                }

                $post->tags()->attach($tag);
            }
        }

        $this->info("SUCCESS!! Post published :]");
        
        return Command::SUCCESS;
    }
}
