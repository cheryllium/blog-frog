<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\FrogPost;
use App\Models\Tag;

class EditFrogPost extends Command
{
    /**
     * the name and signature of the console command.
     * 
     * @var string
     */
    protected $signature = 'frog:edit-tags {slug}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Edit the tags of an existing blog post";

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $slug = trim($this->argument('slug'));

        if(!FrogPost::where('slug', $slug)->exists()) {
            $this->error("There is no post with that slug.");
            return 1;
        }
        $post = FrogPost::where('slug', $slug)->first(); 

        if($post->type != 'post') {
            $this->error("This post is a page, not a blog post. Only blog posts can have tags.");
            return 1; 
        }
        
        $tags = implode(',', $post->tags()->orderBy('tag')->pluck('tag')->toArray());
        if($tags) {
            $this->info("The post currently has the following tags: {$tags}");
        } else {
            $this->info("The post currently has no tags.");
        }

        if($this->confirm("Would you like to add tags to the post?")) {
            $tags = $this->ask("Enter tags to add separated by commas");
            $tags = explode(',', $tags);
            foreach($tags as $tag) {
                $tag = strtolower(trim($tag));

                if($post->tags()->where('tag', $tag)->exists()) {
                    continue; // post already has the tag
                }

                if(Tag::where('tag', $tag)->exists()) {
                    $tag = Tag::where('tag', $tag)->first(); 
                } else {
                    $tag = new Tag(['tag' => $tag]);
                    $tag->save(); 
                }

                $post->tags()->attach($tag);
            }

            $this->info("SUCCESS!! Tags added."); 
        } else if($this->confirm("Would you like to remove tags from the post?")) {
            $tags_to_delete = $this->ask("Enter tags to delete separated by commas");
            $tags_to_delete = explode(',', $tags_to_delete); 

            foreach($tags_to_delete as $tag) {
                $tag = Tag::where('tag', $tag)->first();
                $post->tags()->detach($tag); 
            }

            $this->info("SUCCESS!! Tags deleted."); 
        }

    }
}
