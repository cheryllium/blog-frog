<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\FrogPost;
use Storage;

class DeleteFrogPost extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'frog:delete {slug}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Permanently deletes a blog post';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $slug = trim($this->argument('slug'));

        // Check that there is a post with this slug
        if(!FrogPost::where('slug', $slug)->exists()) {
            $this->error("No post found with the slug: {$slug}");
            return 1; 
        }

        if($this->confirm("This action will PERMANENTLY DELETE the post including the entire content. There is no undo. Are you sure you want to proceed?")) {
            // Delete post
            Storage::delete("/published/{$slug}.md");

            // Delete DB row
            $post = FrogPost::where('slug', $slug)->first();
            $post->delete();

            $this->line("Post deleted.");
        }
        
        return Command::SUCCESS;
    }
}
