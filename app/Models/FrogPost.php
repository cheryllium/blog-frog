<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Tag;
use Storage;

class FrogPost extends Model
{
    use HasFactory;

    protected $fillable = [
        'slug', 'type'
    ]; 

    /**
     * Returns info from file as associative array containing
     * the keys: filepath, title, content
     */ 
    public function getFileInfo()
    {
        $filepath = "/published/{$this->slug}.md";
        
        $file_contents = Storage::get($filepath);
        $split = explode("\n", $file_contents, 2);
        
        $title = trim(explode("#", $split[0])[1]);
        $content = trim($split[1]);
        
        if(strlen($content) > 300) {
            $arr = preg_split("/\r\n|\n|\r/", $content); 
            $count = 0; 
            foreach($arr as $elem) {
                $count += strlen($elem) + 1;
                if($count >= 300) {
                    break; 
                }
            }
            $content_preview = substr($content, 0, $count);;
            $has_readmore = true; 
        } else {
            $content_preview = $content;
            $has_readmore = false; 
        }
        
        return [
            'filepath' => $filepath,
            'title' => $title,
            'content' => $content,
            'preview' => $content_preview,
            'readmore' => $has_readmore,
        ];
    }

    /**
     * Returns relevant post object info
     */
    public function getPostObject() {
        $object = new \stdClass();

        $object->id = $this->id; 
        $object->created_at = $this->created_at;
        $object->slug = $this->slug;

        $info = $this->getFileInfo();
        $object->title = $info['title'];
        $object->content = $info['content'];
        $object->preview = $info['preview'];
        $object->readmore = $info['readmore']; 

        $object->tags = $this->tags()->orderBy('tag')->pluck('tag')->toArray();

        return $object;
    }
    
    /**
     * The tags that belong to this post.
     */
    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'frog_post_tag', 'frog_post_id', 'tag_id');
    }

}
