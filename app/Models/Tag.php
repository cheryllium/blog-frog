<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\FrogPost;

class Tag extends Model
{
    use HasFactory;

    protected $fillable = ['tag'];
    
    /**
     * The posts that are tagged with this tag.
     */
    public function posts()
    {
        return $this->belongsToMany(FrogPost::class, 'frog_post_tag', 'tag_id', 'frog_post_id');
    }
}
