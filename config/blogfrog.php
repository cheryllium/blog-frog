<?php

return [
    'site_name' => env('SITE_NAME', 'My Blog'),
    'site_theme' => env('SITE_THEME', 'default'),
];
