# BLOG FROG

*A hip and hoppy blog engine*

--- 

## how-to

### configuring your blog

The name of the blog can be configured with `SITE_NAME` in the `.env` file. Put it in double quotes if it contains special characters.

The site navigation will automatically display links to all of your pages. When publishing a post, you have the option to publish it as a page instead - more on this in the next section. 

### writing and publishing posts

Start by drafting a post in `/storage/app/drafts/`. Posts should be markdown files named with the desired post slug.

Every post file should start with a header that will be used as the post title. For example, here's `example-post.md`: 

```
# This Is An Example

Lorem ipsum dolor sit amet...
```

When ready to publish, run the `frog:publish` command from the project root, passing in the slug.

```
php artisan frog:publish example-post
```

The command will ask you if you want to add any tags. 

```
$ php artisan frog:publish example-post
Publishing post with the following attributes...
Title: This Is An Example
Slug: example-post

 Would you like to add tags to the post? (yes/no) [no]:
 > yes

 Enter tags separated by commas::
 > example, readme, foobar

SUCCESS!! Post published :]
```

To publish a post as a web page instead of a blog post, use the `--page` tag: 

```
php artisan frog:publish about-this-blog --page
```

Pages can be linked to but will not show up on the feed.

## server configuration notes

The `PROXY_URL` and `PROXY_SCHEME` can be used to force correct base URLs for routing when running behind a reverse proxy. 

