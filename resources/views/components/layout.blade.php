<!doctype html>
<html>
  <head>
    <title>Title of Blog</title>

    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

	  <link href="https://fonts.googleapis.com/css?family=Dosis|Fira+Mono|Ubuntu:300,400,500,700" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset("/theme/default/styles.css") }}">
  </head>

  <body>
    <div class="blogfrog-topborder"></div>
      
    <!-- Navigation bar: desktop  -->
	  <div class="blogfrog-sidebar">
      <a class="logo" href="/">
        <img src="{{ asset("/theme/default/logo.png") }}" />
      </a>
      <h1>{{ config('blogfrog.site_name') }}</h1>
      @foreach ($pages as $page)
          <a href="{{ route('blogfrog.page', $page['slug']) }}">{{ $page['title'] }}</a>
      @endforeach
      <a href="{{ route('blogfrog.rss') }}">rss</a>
	  </div>

    <!-- Navigation bar: mobile -->
    <div class="blogfrog-navbar">
      <a href="/">
        <img src="{{ asset("/theme/default/logo.png") }}" class="logo" />
      </a>
      <h1>Site Title</h1>
          @foreach ($pages as $page)
              <a href="{{ route('blogfrog.page', $page['slug']) }}">{{ $page['title'] }}</a>
          @endforeach
          <a href="{{ route('blogfrog.rss') }}">rss</a>
      </div>
    </div>
    
    <main class="blogfrog-main">
      {{ $slot }}
    </main>
    
    <div class="blogfrog-footer">
        &copy; Footer content
    </div>

  </body>
</html>
