<x-dynamic-component :component="$layoutComponent">
  <x-dynamic-component
      :component="$postComponent"
      :post="$post"
  />
</x-dynamic-component>
