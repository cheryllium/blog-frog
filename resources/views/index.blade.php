<x-dynamic-component :component="$layoutComponent">
  @foreach ($posts as $i => $post)
    <x-dynamic-component
        :component="$postPreviewComponent"
        :i="$i"
        :posts="$posts"
        :post="$post"
        :paginator="$paginator"
    />
  @endforeach
</x-dynamic-component>
