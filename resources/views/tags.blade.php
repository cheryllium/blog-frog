<x-dynamic-component :component="$layoutComponent">
  <h1>Tags</h1>

  <ul class='tags-list'>
  @foreach ($tags as $tag)
    <li>
      <a href="{{ route('blogfrog.tagged', $tag) }}">{{ $tag }}</a>
    </li>
  @endforeach
  </ul>
  
</x-dynamic-component>
