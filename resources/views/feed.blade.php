<?php
'<?xml version="1.0" encoding="UTF-8"?>'.PHP_EOL
?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
    <channel>
        <title><![CDATA[ {{ config('blogfrog.site_name') }} ]]></title>
        <description><![CDATA[ a BLOG FROG blog ]]></description>
        <link><![CDATA[ {{ route('blogfrog.rss') }} ]]></link>
        <language>en</language>
        <pubDate>{{ date('r', now()->getTimestamp()) }}</pubDate>
        <atom:link href="{{ route('blogfrog.rss') }}" rel="self" type="application/rss+xml" />
        
        @foreach($posts as $post)
            <item>
                <title><![CDATA[{{ $post->getFileInfo()['title'] }}]]></title>
                <link>{{ route('blogfrog.post', $post->slug) }}</link>
                <description><![CDATA[{!! $post->getFileInfo()['content'] !!}]]></description>
                <guid>{{ route('blogfrog.post', $post->slug) }}</guid>
                <pubDate>{{ date('r', $post->created_at->getTimestamp()) }}</pubDate>
            </item>
        @endforeach
    </channel>
</rss>
