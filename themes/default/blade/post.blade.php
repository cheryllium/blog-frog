<article class="blogfrog-post">
  <h1 class="blogfrog-post-title">
    {{ $post->title }}
  </h1>

  <div class="blogfrog-post-content-container">
    <!-- Display the post content -->
    <div class="blogfrog-post-content">
      @markdown($post->content)
    </div>

    <hr />
    
    <!-- Display author, date and tags -->
    <div class="blogfrog-post-info">
      <span>Published: {{ $post->created_at }}</span>

      @if (count($post->tags) > 0)
        <span>
          - Tags: 
          @foreach ($post->tags as $tag)
            <a href="{{ route('blogfrog.tagged', $tag) }}">{{ $tag }}</a>
          @endforeach
        </span>
      @endif
    </div>
  </div>
</article>
