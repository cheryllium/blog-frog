<!doctype html>
<html>
  <head>
    <title>{{ config('blogfrog.site_name') }}</title>

    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

	  <link href="https://fonts.googleapis.com/css?family=Dosis|Fira+Mono|Ubuntu:300,400,500,700" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset("/themes/default/styles.css") }}">
    <link rel="stylesheet" href="{{ asset("/rainbow/css/obsidian.css") }}">
  </head>

  <body>
    <div class="blogfrog-topborder"></div>
      
    <!-- Navigation bar: desktop  -->
	  <div class="blogfrog-sidebar">
      <a class="logo" href="/">
        <img src="{{ asset("/themes/default/logo.png") }}" />
      </a>
      <h1>{{ config('blogfrog.site_name') }}</h1>
      @foreach ($pages as $page)
          <a href="{{ route('blogfrog.page', $page['slug']) }}">{{ $page['title'] }}</a>
      @endforeach
      <a href="{{ route('blogfrog.tags') }}">tags</a>
      <a href="{{ route('blogfrog.rss') }}">rss</a>
	  </div>

    <!-- Navigation bar: mobile -->
    <div class="blogfrog-navbar">
      <a href="/">
        <img src="{{ asset("/themes/default/logo.png") }}" class="logo" />
      </a>
      <h1>Site Title</h1>
          @foreach ($pages as $page)
              <a href="{{ route('blogfrog.page', $page['slug']) }}">{{ $page['title'] }}</a>
          @endforeach
          <a href="{{ route('blogfrog.tags') }}">tags</a>
          <a href="{{ route('blogfrog.rss') }}">rss</a>
      </div>
    </div>
    
    <main class="blogfrog-main">
      <div class="blogfrog-main-page">
        {{ $slot }}
      </div>
    </main>
    
    <div class="blogfrog-footer">
        &copy; {{ config('blogfrog.site_name') }} | powered by <a href="https://gitlab.com/cheryllium/blog-frog">BLOG FROG</a>
    </div>

    <script src="{{ asset("/rainbow/js/rainbow.min.js") }}"></script>
  </body>
</html>
