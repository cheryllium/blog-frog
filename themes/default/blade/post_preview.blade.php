<article class="blogfrog-post">
  <a href="{{ route('blogfrog.post', $post->slug) }}">
    <h1 class="blogfrog-post-title">
      {{ $post->title }}
    </h1>
  </a>

  <div class="blogfrog-post-content-container">
    <div>
      @markdown($post->content)
    </div>

    <hr />
    
    <div class="blogfrog-post-info">
      <span>Published: {{ $post->created_at }}</span>
      @if (count($post->tags) > 0)
        <span>
          - Tags:
          @foreach ($post->tags as $tag)
            <a href="{{ route('blogfrog.tagged', $tag) }}">{{ $tag }}</a>
          @endforeach
        </span>
      @endif
    </div>
    
    <!-- replace with x-dynamic-component for pagination -->
    @if ($i == count($posts) - 1 && $paginator->hasPages())
      <x-dynamic-component
          :component="$paginationComponent"
          :paginator="$paginator"
      />
    @endif
    
  </div>
</article>
