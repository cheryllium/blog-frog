# Custom Themes

BLOG FROG comes with a default theme but also supports custom themes. This document describes how to create, register, and use a custom theme. 

## Overview

All themes are located in the `themes/` directory at the project root. This is also where the default theme lives, in `themes/default`. You will choose a name to identify your custom theme and place it in its own directory, `themes/{name-of-theme}`. 

A theme has two subdirectories: 

- `blade` contains the required Blade components (more on these later)
- `assets` contains static assets such as images and CSS files

The following command registers a theme, given its name:

```
php artisan frog:register-theme name-of-theme
```

Under the hood, this command creates symlinks inside the Laravel app so that BLOG FROG can discover the theme. The following symlinks are created: 

- A symlink to the theme's `blade` directory is created in `resources/views/components/themes/{name-of-theme}/`
- A symlink to the theme's `assets` directory is created in `public/themes/{name-of-theme}/`

Once a theme has been registered, it can be used by setting the `SITE_THEME` environmental variable, for example in the `.env` file at the project root. 

```
SITE_THEME=name-of-theme
```

## Blade Components

Each theme requires four Blade components, which are templating files used as "building blocks" for the theme. Each component is a view for a specific thing: 

- `layout.blade.php` - the base layout for all pages
- `post.blade.php` - displays a single, entire post (or site page)
- `post_preview.blade.php` - displays a single post preview, and is used on the home page
- `pagination` - displays the pagination

You can use a component in another component file by using the `x-dynamic-component` tag. For example, you can use the pagination component in another file as follows: 

```
    <x-dynamic-component
        :component="$paginationComponent"
        :paginator="$paginator"
    />
```

**NOTE:** *Due to the way components are nested, the paginator component is only available for use in your post preview component. This may be fixed in a future version of BLOG FROG.*

The `:component` may be one of: `$layoutComponent`, `$postComponent`, `$postPreviewComponent`, or `$paginationComponent`

Each component will receive data depending on which page is being viewed. You can pass this data into child components using `:key="$variable"` as in the example above. 

Below is an overview of what components are used, and what data will be available to them, on each page. Throughout, a **post object** refers to an object representing a post, with the following attributes: 

- `id` - primary key (used internally)
- `created_at` - time post was published
- `slug` - post slug
- `title` - post title
- `content` - post body (Markdown)
- `tags` - post tags (array of strings)

### Home page and Tagged posts page

These pages both display a list of posts, which utilize the post preview component. The following variables are available to the page: 

- `$posts` - A list of posts objects
- `$paginator` - the Paginator object (can be checked for and displayed in your layout component.)

The list of posts is rendered as post preview components in a for loop, and each recieves: 

- The `$posts` and `$paginator` variables available to the page
- `$post` - the current item in `$posts`
- `$i` - the current (integer) loop index

### Posts page (also displays "pages")

The posts page displays a single blog post or web page (for posts published as page.) Each post receives the following data: 

- `$post` - the post object

## Additional notes

### Rendering Markdown & Syntax Highlighting

Markdown is currently rendered in the templates using a custom Markdown directive:

```
@markdown($post->content)
```

Syntax highlighting is powered by Rainbow, and the files are found in the `public/rainbow/` directory. To enable, first include the JS file: 

```
    <script src="{{ asset("/rainbow/js/rainbow.min.js") }}"></script>
```

Then select a CSS file with the color scheme you'd like from the `public/rainbow/css/` directory: 

```
    <link rel="stylesheet" href="{{ asset("/rainbow/css/obsidian.css") }}">
```
