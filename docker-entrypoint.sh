#!/bin/bash

set -e

chown -R www-data:www-data /var/www/html/storage

envsubst '${SERVER_NAME}' < /var/www/html/apache.conf > /etc/apache2/sites-available/000-default.conf

php artisan migrate

/usr/local/bin/docker-php-entrypoint apache2-foreground
